# Diceware

A simple utility for generating easy-to-remember and secure passwords.

## Basic usage

The default settings should be good enough for most people's requirements:

```
> pip install -r requirements.txt
> ./diceware
foresee size sounds overruled sagacity probably comforted

Picked 7 words to achieve at least 80 bits of entropy.
Generated passphrase has 88.28 bits of entropy.
This would take approximately 10,816,097 years to crack.
```

If you'd like to adjust the number of words, use the `-n` flag:

```
> ./diceware -n 10
pay lawfully foresee able apologised comparisons conjunction humiliation sufferer elizabeth

Generated passphrase has 126.11 bits of entropy.
This would take approximately 2,641,916,438,299,690,496 years to crack.
```

If you'd just like the password without the additional statistics, use the `-q`
or `--quiet` flags:

```
> ./diceware -q
pair fifty circle survivor elopement rejoice carry
```

## Alternative word lists

The default word list is pulled from *Pride and Prejudice*. You can use the `-l`
or `--wordlist` flags to pass a file containing a different wordlist instead.
This file should contain one word per line. The entropy/time estimates will be
updated accordingly:

```
> ./diceware -l wordlists/montecristo.txt
worthy stove stillness parties impulses listlessly

Picked 6 words to achieve at least 80 bits of entropy.
Generated passphrase has 83.74 bits of entropy.
This would take approximately 232,775 years to crack.
```

If you'd like to generate a custom word list, the `generate_wordlist.py` script
is provided. It takes a plain text file and extracts all words consisting only
of letters. The resulting output can then be passed in with the `-l/--wordlist`
flags.

## Miscellaneous notes

* Entropy is computed based on the selection process, _not_ based on the number
  of characters (which is a highly flawed way of measuring password quality).
* Time cracking estimates are computed assuming an adversary that can try 2^40
  hashes per second. The actual figure obviously depends on a variety of
  factors, so these estimates should be taken as guidelines rather than any sort
  of firm measure of security.
* It's worth remembering that rolling passwords repeatedly until you find one
  you like naturally decreases the amount of entropy by approximately
  lg(number of reroll) bits. In practice, unless you're rolling an extremely
  large number of times, this shouldn't affect the security of your password too
  strongly, but it's worth keeping in mind.