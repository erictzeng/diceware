#!/usr/bin/env python
from __future__ import division
from __future__ import print_function

import math
import random

import click


@click.command()
@click.option('--wordlist', '-l', type=click.File('r'),
              default='wordlists/prideandprejudice.txt')
@click.option('-n', type=click.IntRange(0, None))
@click.option('-v/-q', '--verbose/--quiet', default=True)
@click.option('--replace/--no-replace', default=False)
def diceware(wordlist, n, verbose, replace):
    sysrand = random.SystemRandom()
    words = list(word for word in (word.rstrip() for word in wordlist) if word)
    if n is None:
        pick = int(math.ceil(80 / math.log(len(words), 2)))
    else:
        pick = n
    if replace:
        phrase = (sysrand.choice(words) for i in range(pick))
    else:
        phrase = (sysrand.sample(words, pick))
    print(' '.join(phrase))
    if verbose:
        print()
        if n is None:
            print('Picked {} words to achieve at least 80 bits of entropy.'
                  .format(pick))
        bits, years_to_crack = diagnostics(words, pick, replace=replace)
        print('Generated passphrase has {:.2f} bits of entropy.'.format(bits))
        print('This would take approximately {:,.0f} years to crack.'
              .format(years_to_crack))


def diagnostics(words, n, replace=False):
    if replace:
        bits = math.log(len(words), 2) * n
    else:
        bits = sum(math.log(len(words) - i, 2) for i in range(n))
    # going off of 2 ** 40 guesses per second...
    seconds_to_crack = 2 ** max(bits - 40 - 1, 0)
    years_to_crack = seconds_to_crack / (60 * 60 * 24 * 365)
    return bits, years_to_crack


if __name__ == '__main__':
    diceware()
