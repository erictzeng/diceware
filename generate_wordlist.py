import re

import click


@click.command()
@click.argument('source', type=click.File('r'))
@click.argument('output', type=click.File('w'))
def generate_wordlist(source, output):
    match = re.findall(r'[a-zA-Z]+', source.read().lower(), flags=re.MULTILINE)
    words = sorted(set(match))
    output.write('\n'.join(words))
    output.write('\n')
    click.echo('Wrote {}-word list to {}.'.format(len(words), output.name))


if __name__ == '__main__':
    generate_wordlist()
